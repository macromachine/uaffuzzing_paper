\section{Approach}\label{sec:approach}

In this section, we elaborate the main components of Fig.~\ref{fig:workflow}: static analysis, feedback mechanism, and the fuzzing loop.

\subsection{Static Analysis}
The purpose of static analysis is to capture the memory free and use pairs, which operate on the same memory and the memory use is behind its free in program control flow graph.
With the memory free and use pairs identified, it can help the fuzzer focus the resources on the vulnerable program codes.

To identify the memory free and use pairs, we take advantage of the existing pointer-based static analysis techniques, such as SVF and Pinpoint. 
First, we use the pointer analysis, e.g., Andersen algorithm, to generate the pointer-to information, and build an interprocedural memory static-single assignment (SSA).
With the memory SSA, we can capture the value-flow chain of pointer variables.
For any memory free and use pairs in a value-flow chain, we conduct their reachability analysis in the program control flow graph.
If the memory free can reach its use, they construct a memory free and use pair.
\chx{Haijun: please elaborate this analysis as detailed as possible; refer to other papers if necessary.
Also, there seems no mentioning about how these information is encoded/instrumented.}

\hai{may be in introduction}. The static analysis can capture the memory free and use pairs, however, they must not be the true use-after-free vulnerabilities.
First, the memory on which the free and use operate may be different.
The static analysis cannot precisely differentiate the memory due to the alias, complex data and so on. Second, the memory free and use may lie in the infeasible program paths.
Although some work incorporates extra techniques, e.g., spatio-temporal context reduction and symbolic execution, to reduce the false positives, there are still challenges in scalability and accuracy.
Additionally, to debug and fix the use-after-free vulnerability, it would be better to provide a test case triggering the use-after-free.

Based on these issues, we leverage the static analysis to provide the context of use-after-free vulnerabilities, and use the fuzzing system to trigger the true use-after-free.

\subsection{Feedback Mechanism} \label{sec:instru}
When providing the context of use-after-free by static analysis, we use the fuzzing system to trigger the true use-after-free vulnerability.
In coverage-guided fuzzers, an important component is the feedback mechanism.
In this section, we introduce the feedback specific to the use-after-free: collective temporal and selective spatial instrumentation.

\LinesNumbered
\begin{algorithm}[tb]
	\small
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output}
	\SetKwComment{Comment}{$\quad //$}{}
	\Input{a program $P$, and set of free and use pairs $S$}
	\Output{the instrumented program $P'$}
	\BlankLine
	\ForEach{pair $\langle f_i, u_i \rangle \in S$}{ \label{instru:begin}
	    compute the control dependencies $C_f$ of memory free $f_i$\; \label{instru:f_begin}
	    \ForEach{statement $c_j \in C_f$}{   
	        \ForEach{immediately previous statement $c_p$ of $c_j$}{
	            let $k_{p}$ and $k_j$ be \texttt{identities} of $c_p$ and $c_j$\;
	            instrument the branch $k_{p}\oplus k_j$\; \label{instru:f_end}
	        }
	    }
	    compute the control dependencies of $C_u$ of memory use $u_i$\; \label{instru:u_begin}
	    \ForEach{statement $c_j \in C_u$}{    
	        \ForEach{immediately previous statement $c_p$ of $c_j$}{
	            let $k_f$, $k_p$, $k_j$ be \texttt{identities} of $f_i$, $c_p$, $c_j$\;
	            \If{memory free $f_i$ is executed}{
	                instrument the branch $k_f \oplus k_p \oplus k_j$\;  \label{instru:u_end} \label{instru:end}
	            }
	        }
	    }
	}
	\caption{\texttt{Collective Temporal Instrument}}
	\label{alg:instru}
\end{algorithm}

\chx{put problems inside Motivatin example section; put solutions in Approach}

\noindent \textbf{Collective Temporal Instrumentation.} 
The existing coverage-guided fuzzers are ineffective for detecting use-after-free vulnerabilities, as they do not encode memory free information into the memory use.
To address this issue, we design the collective temporal instrumentation specific to use-after-free detection, as shown in Algorithm~\ref{alg:instru}.

The algorithm takes as input a program $P$ and a set of free and use pairs $S:=\{\langle f_i, u_i \rangle | 0 \leq i < n\}$, and outputs the instrumented program $P'$.
For each pair $\langle f_i, u_i \rangle$ in $S$ (Line~\ref{instru:begin}), we instrument the memory free and use differently, as shown at Lines~\ref{instru:f_begin}-\ref{instru:f_end} and Lines~\ref{instru:u_begin}-\ref{instru:u_end}.
Considering the memory free $f_i$, we first compute its control dependencies $C_f$ at Line $2$, which control whether $f_i$ is executed.
Then, for each control dependence statement $c_j$ in $C_f$ (Line $3$), we instrument it as existing coverage-guided fuzzers (Lines $4$-$6$), e.g., AFL.
At Line $4$, we identify the immediately previous statement $c_p$ of $c_j$, and let $c_p$ and $c_j$ represent the \texttt{identities} of $k_p$ and $k_j$ (Line $5$).
Our approach computes $k_p \oplus k_j$, and uses it to represent the branch $\langle c_p, c_j \rangle$ (Line $6$).
For the memory use $u_i$, we need consider the corresponding memory free information.
We also first compute the control dependencies $C_u$ of memory use $u_i$ at Line $7$.
For each control dependence $c_j$ in $C_u$ (Line $8$), we design a new mechanism to instrument it, as shown at Lines $9$-$12$.
At Line $9$,  we identify the immediately previous statement $c_p$ of $c_j$, and let $k_f$, $k_p$, $k_j$ represent the \texttt{identities} of $f_i$, $c_p$, $c_j$ (Line $10$).
In particular, we extract the information of memory free $f_i$.
If the memory free $f_i$ is executed, we compute $k_f \oplus k_p \oplus k_j$, and use it to represent the branch $\langle c_p, c_j \rangle$ (Line $12$).
From $k_f \oplus k_p \oplus k_j$, we can see that the memory free information is encoded.
Through incorporating $k_f$, the branch (indicated by $k_p \oplus k_j$) is actually evolved into multiple branches to encode the memory free information.

\noindent \textbf{Selective Spatial Instrumentation.}
To improve the efficiency, some program codes irrelevant to the use-after-free vulnerabilities should not be considered.
For example, the Lines $10$-$11$ in Fig.~\ref{fig:m_example} are irrelevant to the use-after-free vulnerabilities.
Thus, we would not instrument these program codes.
If program codes are not instrumented to provide the feedback, it means that they are not used to test mutation in our approach. 
For example, two test cases that execute the true and false branch of Line $10$ in Fig.~\ref{fig:m_example} respectively are considered equivalent.
We only leverage one of these two cases to further generate new test cases.
As a result, our approach focuses more resources on vulnerable codes of use-after-free vulnerabilities.

\subsection{Fuzzing Loop}

\chx{may add comparison w.r.t. the feedback}

\LinesNumbered
\begin{algorithm}[tb]
	\small
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output} 
	\SetKwComment{Comment}{$\quad //$}{}
	\Input{an instrumented program $P$, and set of initial seeds $T$}
	\Output{set of test cases $T_{\mathbf{\times}}$ triggering use-after-free}
	\BlankLine
    \While{time budget not reached and abort signal not received}{
        $t \leftarrow \mathit{ChooseNext(T)}$\;
        $mask \leftarrow \mathit{InformationFlow(t)}$\;
        \ForEach{deterministic mutation operation}{
            \For{$0 \leq i < |t|$}{
                \If{$i \in mask$}{
                    $t_n \leftarrow$ apply the deterministic mutation\;
                    \If{$t_n$ triggers use-after-free}{
                       $T_{\mathbf{\times}} \leftarrow  T_{\mathbf{\times}} \cup \{t_n\}$\;
                    }
                    \ElseIf{$t_n$ is interesting}{
                        $T  \leftarrow  T \cup \{t_n\}$\;
                    }
                }
            }
        }
        $p \leftarrow AssignEnergy(t)$\;
        \For{$0 \leq i < p$}{
            $t_n \leftarrow t$\;
            $num \leftarrow \mathit{RandomMutationNum}$\;
            \For{$0 \leq j < num$}{
                $mutation \leftarrow \mathit{RandomMutationType}$\;
                $position \leftarrow \mathit{RandomMutationPosition(mask)}$\;
                $t_n \leftarrow \mathit{Mutate}(t_n, position, mutation)$\;
            }
            \If{$t_n$ triggers use-after-free}{
               $T_{\mathbf{\times}} \leftarrow  T_{\mathbf{\times}} \cup \{t_n\}$\;
            }
            \ElseIf{$t_n$ is interesting}{
                $T  \leftarrow  T \cup \{t_n\}$\;
            }
        }
    }
	\caption{\texttt{The Fuzzing Loop}}
	\label{alg:fuzzing}
\end{algorithm}

\chx{how do we write about deterministic mutation operators? I prefer not to mentioning them on Approach, but only point out how to determine whether there is a data/control dependency with flip1 (use some other term for this)}

The fuzzing loop takes the instrumented program and starts fuzzing process.
The instrumentation (Section~\ref{sec:instru}) provides the feedback to determine whether the test cases should be kept or not in fuzzing process.
The Algorithm~\ref{alg:fuzzing} presents the overview of our fuzzing loop.

The algorithm takes as the inputs an instrumented program $P$ and a set of initial seeds $T$, and outputs a set of test cases $T_{\times}$ triggering the use-after-free vulnerabilities.
When the time budget is not reached and no abort signal is received (Line $1$), we continuously perform the mutation operations to generate new test cases (Lines $2$-$23$).
We have two mutation operations: deterministic mutation (Lines $4$-$11$)and \texttt{havoc} mutation (Lines $12$-$23$), the same with existing coverage-guided fuzzers.

At Line $2$, we select a test case $t$ from the test case pool $T$, and then perform the information flow analysis for $t$ (Line $3$).
The information flow analysis determines which bits of $t$ can be mutated towards use-after-free vulnerabilities, and we elaborate it in later section.
For each deterministic mutation operation (Line $4$), e.g., bit flipping, byte flipping, arithmetic increment and decrement, our approach considers its application to each bit of $t$ (Line $5$).
If the bit of $t$ is identified by our information flow analysis (Line $6$), we apply the deterministic mutation to such bit and generate a new test case $t_n$.
If $t_n$ triggers a use-after-free vulnerability, we add it into $T_{\times}$.
Otherwise, we measure whether $t_n$ is an interesting test case.
If a test case achieves the new coverage, it is considered interesting.
Note that, our approach provides a new mechanism of program coverage, as introduced in Section~\ref{sec:instru}.

At Line $12$, we assign the energy power $p$ to mutate the test cases by \texttt{havoc} mutation operation.
The more energy power we assign, the more test cases will be generated by \texttt{havoc} mutation.
The \texttt{havoc} mutation will set random bytes to random values, delete, add or clone subsequences of the input, thus it may generate the unexpected test cases.
At Line $15$, we randomly set the number of mutation operations for test case $t_n$.
For every mutation operation (Line $16$), we randomly decide the mutation type (Line $17$), e.g., set random bytes to random values, delete subsequences of the input, and then select the position where to mutate.
It is worth noting, the position must consistent with the $mask$ identified by information flow analysis.
At Line $19$, we apply the mutation type \texttt{mutation} to the position \texttt{position} of $t_n$.
Then, we add the generated test case $t_n$ into $T_{\times}$ and $T$ as deterministic stage.

\noindent \textbf{Information Flow Analysis.}

\LinesNumbered
\begin{algorithm}[tb]
	\small
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output} 
	\SetKwComment{Comment}{$\quad //$}{}
	\Input{a program $P$, and a test case $t$}
	\Output{the information flow relation $mask$}
	\BlankLine
	extract and instrument variables of conditional statements\;
	run $P$ under $t$, and get execution $E$\;
	$I \leftarrow \emptyset$ \;
	\ForEach{$e_i \in E$}{
	    \If{$e_i$ is control dependency of memory free}{
	        add instrumented information of $e_i$ into $I$ \;
	    }
	    \If{$e_i$ is control dependency of memory use}{
	        \If{the corresponding memory free $e_f$ is executed}{
	            add instrumented information of $e_i$ into $I$ \;
	        }
	    }
	}
	\For{$0 \leq i < |t|$}{
        $t_n \leftarrow$ apply deterministic mutation `flip1'\;
        run $P$ under $t_n$, and get execution $E_n$\;
        $I_n \leftarrow \emptyset$ \;
    	\ForEach{$e_i \in E_n$}{
    	    \If{$e_i$ is control dependency of memory free}{
    	        add instrumented information of $e_i$ into $I_n$ \;
    	    }
    	    \If{$e_i$ is control dependency of memory use}{
    	        \If{the corresponding memory free $e_f$ is executed}{
    	            add instrumented information of $e_i$ into $I_n$ \;
    	        }
    	    }
    	}
        \If{$I$ and $I_n$ are different}{
            $mask \leftarrow mask \cup \{i\}$\;
        }
    }
    \Return $mask$\;
	\caption{\texttt{Information Flow}}
	\label{alg:information}
\end{algorithm}

Our new instrumentation can help the fuzzer generate test cases towards the use-after-free vulnerabilities.
This information alone is not sufficient to efficiently generate the expected test cases.
Another concern is that which bits of test cases are closely relevant to the context of use-after-free vulnerabilities.
One widely adopted solution is taint analysis technique, which monitors the program execution and computes the data flow. 
For example, VUzzer and Angora both use dynamic taint analysis to gather information of comparisons.
Although taint analysis is powerful, it is too heavy for the fast program execution, e.g., fuzzing.
Here, we propose the light information flow analysis to capture the bits of test cases relevant to context of use-after-free vulnerabilities, as shown in Algorithm~\ref{alg:information}.

The algorithm takes as inputs a program $P$ and a test case $t$, and outputs the information flow relation $mask$ under $t$.
At Line $1$, we extract and instrument the variables of conditional statements, and then run $P$ under $t$ to get the execution $E$ (Line $2$).
First, we obtain the information $I$ of instrumented variables relevant to use-after-free vulnerabilities, as shown at Lines $4$-$9$.
For each statement instance $e_i$ in $E$ (Line $4$), if $e_i$ is a control dependency of a memory free (Line $5$), we add its instrumented variable information into $I$ (Line $6$).
On the other hand, if $e_i$ is a control dependency of a memory use (Line $7$), and we additionally consider whether its corresponding memory free is executed (Line $8$).
If above two conditions are satisfied, we add its instrumented variable information into $I$ (Line $9$).
At Lines $10$-$21$, we mutate the test case $t$ bit-by-bit, and check whether the instrumented variable information is changed.
If so, the corresponding bit of $t$ is relevant to the context of use-after-free vulnerabilities.
At Line $11$, we apply the deterministic mutation `flip1' to $t$ to generate new test case $t_n$.
Then, we run $P$ under $t_n$, and get its execution trace $E_n$.
At Lines $14$-$19$, we get the instrumented information $I_n$ of $E_n$ relevant to use-after-free vulnerability, the same with Lines $4$-$9$.
If the instrumented information $I$ and $I_n$ are different (Line $20$), the corresponding bit actually affects the variables relevant to use-after-free vulnerability.
Thus, we add the corresponding bit $i$ into $mask$.

Note that, our instrumentation and fuzzing loop seemingly consider only the control dependencies.
How does our approach handle the situation where the use-after-free vulnerabilities relevant to data dependencies.
For example, in Fig.~\ref{fig:m_example} Lines $8$-$9$ are data dependent by Line $12$.
Thus, they are relevant to use-after-free vulnerability by data dependencies.
In fact, our approach implicitly considers the data dependencies.
In this example, our approach captures that the fourth and fifth bits of input are relevant to variables of Line $12$.
Thus, our approach mutates these two bits towards the use-after-free vulnerability.
In the sense of information flow analysis, we actually consider the data dependencies relevant to use-after-free vulnerabilities.


