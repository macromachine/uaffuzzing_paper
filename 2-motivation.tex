\section{Approach Overview}\label{sec:overview}
In this section, we present a motivating example, and then introduce the overview of our approach.


\begin{figure}[t]
	\centering
	%\setlength{\belowcaptionskip}{-11pt}
	\includegraphics[scale=0.98]{Fig/example.pdf}
	\caption{An example of use-after-free vulnerability}
	\label{fig:m_example}
\end{figure}


\subsection{Motivating Example}

\begin{figure*}[t]
	\centering
	%\setlength{\belowcaptionskip}{-11pt}
	\includegraphics[scale=0.38]{Fig/afl_flow.pdf}
	\caption{Fuzzing process of AFL in the example of Fig.~\ref{fig:m_example}}
	\label{fig:afl_flow}
\end{figure*}

Fig.~\ref{fig:m_example} shows a motivating example simplified from program \textit{readelf}, which contains the use-after-free vulnerability (e.g., CVE-2018-20632).
The program first allocates a block of memory at Line $4$, which is pointed by the pointer $ptr1$.
At Line $9$, the pointer $ptr1$ is assigned to $ptr2$, and thus $ptr1$ and $ptr2$ are alias pointers.
Then, it frees the memory pointed by $ptr1$ at Line $12$, which actually frees the memory pointed by $ptr2$ as well.
At Line $16$, it uses the memory pointed by $ptr2$, resulting in a use-after-free vulnerability. 

\subsection{Coverage-based Fuzzers}
Existing coverage-based fuzzers (e.g., AFL, AFLFast and FairFuzz) usually use CFG edges to guide the fuzzing process.
Given a test case, fuzzers mutate it and generate some new test cases. 
If the new test case covers the CFG edges uncovered by previous test cases, it is considered as interesting and added into the test pool, otherwise, it is discarded.
The test cases in the test pool are then popped out for the further mutation, thereby more and more test cases are generated.

When applying existing coverage-based fuzzers, such as AFL, to detect the use-after-free in example of Fig.~\ref{fig:m_example}, we assume the initial seed is $\textit{buf}_0=$`$aaaaaaa$'.
Then, AFL mutates $\textit{buf}_0$ and generate test cases $\textit{buf}_1=$`$aaasaaa$', $\textit{buf}_2=$`$aaaaeaa$' and $\textit{buf}_3=$`$aaaaaea$' shown in Fig.~\ref{fig:afl_flow} (b), (c) and (d), respectively.
Then, AFL mutates $\textit{buf}_1$ into $\textit{buf}_4=$`$auasaaa$', $\textit{buf}_2$ into $\textit{buf}_5=$`$aaraeaa$' and $\textit{buf}_5$ into $\textit{buf}_6=$`$f\backslash xe1raeaa$'.
We actually run AFL on this example, and do not find the use-after-free vulnerability with 24 hours.

In Fig.~\ref{fig:afl_flow}, we can see that AFL can generate test cases to cover all 
individual statements relevant to use-after-free vulnerability, e.g., Lines 9, 12 and 16.
However, there is no a single test case that covers Lines 9, 12 and 16 simultaneously, because AFL uses individual CFG edges as the feedback to guide fuzzing process.
However, many vulnerabilities, e.g., use-after-free, happen under the context of a  program (e.g., \textit{malloc} $\rightarrow$ \textit{free} $\rightarrow$ \textit{use}), which requires multiple CFG edges are covered simultaneously by a single test case.
To detect these vulnerabilities, we develop the value-flow feedback mechanism to guide the fuzzing process, which can cover the on-demand context of the program.
 
\subsection{Overview of \tool}
\begin{figure}[t]
\centering
%\setlength{\belowcaptionskip}{-11pt}
\includegraphics[scale=0.4]{Fig/overview.pdf}
\caption{Overview of Our Approach}
\label{fig:workflow}
\end{figure}

Complementary to coverage-based fuzzers, \tool is to detect the vulnerabilities that happen under the context of a program, e.g., use-after-free.
Fig.~\ref{fig:workflow} shows the overview of \tool, consisting of three main components: static analysis, instrumentation and the fuzzing loop.

\subsubsection{Static Analysis}
The static analysis component statically analyzes a program, and captures the context of target vulnerabilities in the program.
For example, the context of use-after-free vulnerability is \textit{malloc} $\rightarrow$ \textit{free} $\rightarrow$ \textit{use}, which produces the value-flow from the memory allocation, goes through the memory deallocation, and reaches the memory use. 
Although the static analysis may generate false positives, however, its results are comprehensive.
Thus, the identified value-flows can help guide the dynamic fuzzing technique to expose the true vulnerabilities. 
In Fig.~\ref{fig:m_example}, the static analysis can identify the value-flow Lines $9 \rightarrow 12 \rightarrow 16$.

\subsubsection{Instrumentation}
The instrumentation component provides the feedback to guide the fuzzing process. 
\tool develops two instrumentation strategies. 
The collective temporal instrumentation encodes the value-flows as the feedback, guiding \tool gradually to cover the value-flows so that to detect the vulnerabilities.
The other is the information flow instrumentation, where \tool instruments all the comparison instructions relevant to value-flows, which is used to perform the information flow analysis.

The collective temporal instrumentation is to temporally encode the value-flows into the feedback information.
To illustrate the mechanism of collective temporal instrumentation, we first introduce the instrumentation in coverage-based fuzzers (e.g., AFL).
We take the CFG edge $15 \rightarrow 16$ for example.
AFL first assigns a random value \textit{pre\_location} to Line $15$, and a random value \textit{cur\_location} to Line $16$.
Then, it computes $shared\_mem[cur\_location \oplus prev\_location]++$\textit{++}.

To this end, we differentiate the treatment of memory free and use.
For the memory free, we instrument them as the existing coverage-guided fuzzers, e.g., AFL.
For example, in Fig.~\ref{fig:m_example} we instrument the branches $\langle$5,6$\rangle$ and $\langle$6, 7$\rangle$ as AFL.
Then, we encode the memory free information into its use.
For the memory use, we first determine whether its corresponding memory free is covered.
If so, we instrument the memory use as follows.
Assume the \texttt{identities} for memory free, immediately previous basic block of memory use, and memory use are \textit{fr}, \textit{prev}, and \textit{cur}, respectively. \ting{what do fr, prev, cur mean? Plz explain first.}
The \texttt{identify} uniquely captures a basic block in fuzzing system.
We compute the coverage by incorporating the these three elements.
For example, we compute the branch coverage by: $fr \oplus prev \oplus cur$, however, AFL computes the branch coverage by: $prev \oplus cur$.
%Obviously, our computation encodes the memory free information.
Compared to existing coverage-guided fuzzers, our approach evolve a branch into multiple states by incorporating $fr$.
For example, we would encode the memory free information Line $7$ into the branches $\langle$12, 13$\rangle$, $\langle$13,14$\rangle$, and  $\langle$14, 15$\rangle$. 

To improve the efficiency of fuzzing, selective spatial instrumentation only instruments the relevant program codes to use-after-free vulnerabilities.
For example, in Fig.~\ref{fig:m_example} the branches $\langle$8,9$\rangle$ and $\langle$10,11$\rangle$ are instrumented in our approach.

\noindent \textbf{Fuzzing Loop.}
When finishing the instrumentation, we start the fuzzing loop.
Assume the initial seed is $str_0=$`$aaaaaa$' for the program in Fig.~\ref{fig:m_example}.
In existing coverage-guided fuzzers, e.g., AFL, it first generates the following three test cases: $str_1 =$`$aaasaa$', $str_2 =$`$tttt.t$', $str_3 =$`$_..eea$', and $str_4 =$`$....m.$'.
Then, it further generates two test cases $str_5=$`$arasaa$' and $str_6=$`$uuuue.$' based on existing test cases.
In the third round, it continuously generates a test case $str_7=$`$fuuue.$' based on the existing test cases
So far, AFL covers the memory free (Line $7$) via $str_5$,and memory use (Line $15$) via $str_7$, respectively.
\ting{put the next sentence in the motivation example?}
However, it cannot further proceed to generate the test case covering both Lines $7$ and $15$ in \textbf{10 hours}, because AFL uses only the branch information as the feedback. 

Then, we illustrate how our approach to address this issue.
Our approach runs the program under the initial seed $str_0$, and then mutates $str_0$ with the `flip1' operation (i.e., flipping one bit of $str_0$ bit-by-bit).
After finishing the `flip1' mutation operation, we conduct information flow analysis based on the mutated test cases. \chx{don't be too specific; this strategy may be problematic in practice}
Thereby, we can capture the relationship between the branches and the test case. 
To improve the efficiency, we only use the `flip1' operation to mutate test cases for information flow analysis.
For example, we can capture that the information flow exists between Line $5$ and the $3^{th}$ bit of $str_0$ (indexed from $0^{th}$), Line $12$ and the $4^{th}$, $5^{th}$ bit, respectively.
Equipped with the information flow, our approach can adaptively mutate the input to cover the target.
Thus, our approach continuously mutates the $3^{th}$ and $1^{th}$ bit to generate test cases $str'_1=$`$aaasaa$' and $str'_2=$`$arasaa$', so that to cover Line $7$.
Since the branch $\langle$12,13$\rangle$ encodes the information of Line $7$, our approach would further generate a test cast $str'_3=$`$.r:sea$' based on $str'_2$.
Built on $str'_3$, our approach further generates $str'_4=$`$.rusea$' and $str'_5=$`$frusea$.
As a result, our approach can trigger this use-after-free vulnerability in \textbf{20 minutes}.

